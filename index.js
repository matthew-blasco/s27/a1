const HTTP = require('http');

//2
const PORT = 4000;

// Mock database
let directory = [
    {
        "name": "Brandon",
        "email": "brandon@mail.com"
    },
    {
        "name": "Jobert",
        "email": "jobert@mail.com"
    }
];

HTTP.createServer((req, res) => {
    // console.log("test")
    
    //2
    if(req.url == "/" && req.method == "GET"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to Booking System")

    } else if(req.url == "/profile" && req.method == "GET"){

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to your profile!")
    } else if(req.url == "/courses" && req.method == "GET"){

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Here's our courses available")
    } else if(req.url == "/addcourses" && req.method == "GET"){

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Add a course to our resources")
    } else if(req.url == "/updatecourse" && req.method == "GET"){

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Update a course to our resources")
    } else if(req.url == "/archivecourses" && req.method == "GET"){

        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Archive courses to our resources")
    } else {
        res.writeHead(404, {"Content-Type": "text/plain"})
        res.end("Bad Request. Please do check the url again.")
    }

    

}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));
